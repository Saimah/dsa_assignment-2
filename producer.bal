import ballerinax/kafka;

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

public function main() returns error? {
    string dsa_message = "Good day all, the dsa assignment is due on the 24th October 2021";
    string dpg_message = "Test 2 will be written on 25th October 2021";
    string eap_message = "Our last quiz will be written on the 30th October 2021";
    string dpt_message = "Good day students, note that your test 2 marks will be uploaded tonight.";

    check kafkaProducer->send({
        topic: "dsa",
        value: dsa_message.toBytes()
    });

    check kafkaProducer->send({
        topic: "dpg",
        value: dpg_message.toBytes()
    });

    check kafkaProducer->send({
        topic: "dpt",
        value: dpt_message.toBytes()
    });

    check kafkaProducer->send({
        topic: "eap",
        value: eap_message.toBytes()
    });

    check kafkaProducer->'flush();
}
